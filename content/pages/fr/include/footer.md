Title: Qui sommes-nous ?
lang: fr
status: hidden
is_footer: true

Kresus étant un logiciel libre, nous sommes une [communauté de
développeurs](https://framagit.org/kresusapp/kresus/graphs/master) à l'avoir
créé. Nous ne sommes pas financés par Weboob ou aucune autre société et sommes des
développeurs indépendants, intéressés par le logiciel libre et les outils de
finances personnelles.

Si vous aimez ce qu'on fait, vous pouvez nous faire un don via notre [![Liberapay logo](/images/pages/liberapay.svg)](http://liberapay.com/kresus).