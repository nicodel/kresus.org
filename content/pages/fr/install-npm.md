Title: Installation via npm
Date: 2020-01-02 16:30
Slug: install-npm
Summary: Apprenez à installer Kresus avec npm
toc_run: true
toc_title: Installation via npm&nbsp;
lang: fr
status: hidden

## Prérequis

Il vous faudra `nodejs` et `npm` d'installés sur la machine accueillant Kresus.

Kresus utilise [Weboob](http://weboob.org/) sous le capot, pour se connecter au
site web de votre banque. Vous aurez besoin d'[installer le cœur
Weboob](http://weboob.org/install) afin que l'utilisateur exécutant Kresus
puisse les utiliser. Ce logiciel nécessite Python et a ses propres
dépendences Python.

Kresus nécessite la toute dernière version de Weboob. Bien que Kresus puisse
fonctionner avec de précédentes versions, les modules des banques peuvent être
obsolètes, et la synchronisation avec votre banque pourrait être
dysfonctionnelle.

**AVERTISSEMENT: Il n'y a aucun système d'authentification intégré dans Kresus,
il est donc risqué de l'utiliser tel quel. Assurez-vous bien de mettre en place
un système d'authentification vous-même…**

## Installation

Vous pouvez installer Kresus avec la commande suivante :

    :::bash
    npm install -g kresus

Une fois que vous aurez [mijoté votre configuration]({filename}02-admin.md#avec-un-fichier-configini), Kresus sera
ensuite exécutable avec la commande :

    :::bash
    kresus --config /path/to/config.ini

### Préfixe (installation non globale)

Le paramètre `-g` permet d'installer Kresus globalement, ce qui pourrait
nécessiter les droits super-utilisateur. Une autre possibilité est d'installer
Kresus dans un répertoire à part, avec la commande suivante :

    :::bash
    npm install --prefix /path/to/local/install kresus

Une fois la [configuration prête]({filename}02-admin.md#avec-un-fichier-configini), vous pourrez
lancer Kresus ainsi :

    :::bash
    /path/to/local/install/node_modules/bin/kresus --config /path/to/config.ini
