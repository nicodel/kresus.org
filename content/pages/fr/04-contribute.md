Title: Contribuer
Date: 2017-03-11 10:02
Slug: contribute
Summary: Découvrez toutes les façons de contribuer à Kresus : code, doc, design, traduction et bien d'autres manières !
lang: fr
is_doc: true
status: hidden

Kresus est un logiciel <a
href="https://framagit.org/kresusapp/kresus/blob/master/LICENSE"><strong>libre</strong></a>
et <a
href="https://framagit.org/kresusapp/kresus"><strong>open-source</strong></a>.
Vous pouvez donc y contribuer facilement, quelles que soient vos compétences&nbsp;!

Pour vous aider, voici quelques idées pour commencer&nbsp;:

* Signalez tout problème que vous pourriez rencontrer dans [notre gestionnaire
  de bugs](https://framagit.org/kresusapp/kresus/issues).
* Choisissez un [ticket marqué
  «&nbsp;<em>easy</em>&nbsp;»](https://framagit.org/kresusapp/kresus/issues?label_name%5B%5D=easy)
  et corrigez-le. Ces [indications pour
  contribuer](https://framagit.org/kresusapp/kresus/blob/master/CONTRIBUTING.md)
  pourraient vous être utiles.
* Améliorez le design et l'interface utilisateur de Kresus.
* [Traduisez Kresus](https://framagit.org/kresusapp/kresus/tree/master/shared/locales) dans d'autres langues
* Rédigez des documentations (ce site est libre aussi : [https://framagit.org/kresusapp/kresus.org](https://framagit.org/kresusapp/kresus.org) !)
* Et tout simplement, parlez-en autour de vous&nbsp;!

<em>Note&nbsp;:</em> Les contributeurs de Kresus ont établi un code de
conduite afin que chacun&sdot;e soit bienvenu&sdot;e pour contribuer. Nous vous invitons
à en [prendre connaissance](https://framagit.org/kresusapp/kresus/blob/master/CodeOfConduct.md).
