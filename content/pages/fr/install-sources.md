Title: Installation depuis les sources
Date: 2019-01-02 16:30
Slug: install-sources
lang: fr
status: hidden

Pour installer les dépendances node et compiler les scripts (cela
n'installera pas Kresus globalement) pour un usage en production :

    :::bash
    npm install && npm run build:prod

Vous pourrez alors lancer Kresus en utilisant

    :::bash
    NODE_ENV=production npm run start

_Note :_ Vous pouvez aussi utiliser `npm run build:dev` pour compiler les
scripts en mode de développement (mieux pour débugger mais inadapté pour un
usage en production).
