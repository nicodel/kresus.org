Title: Install with npm
Date: 2020-01-02 16:30
Slug: install-npm
Summary: Learn how to install Kresus with npm
lang: en
status: hidden

## Requirements

You'll need `nodejs` and `npm` installed in the machine hosting Kresus.

Kresus makes use of [Weboob](http://weboob.org/) under the hood, to fetch information from
your bank website. You will have to [install Weboob core and
modules](http://weboob.org/install) so that the user running Kresus can use
them.

Kresus requires the latest stable version of Weboob. Although Kresus could
work with previous versions, bank modules can get obsolete and the
synchronisation with your bank might not work with older versions.

**IMPORTANT NOTE: There is no builtin authentication mechanism in Kresus, it is
then risky to use it as is. Please make sure to set up an authentication system
by yourself.**

## Global install

Alternatively, if you want to install Kresus globally, you can use:

    :::bash
    npm -g install kresus

Once you'll have generated a nice [configuration file]({filename}02-admin.md#with-a-configuration-file),
Kresus will be executable with the command:

    :::bash
    kresus --config /path/to/config.ini

### Prefixed install (local)

The `-g` parameter allows to install Kresus globally, which might require super-user permissions
rights. Another way is to install Kresus in a dedicated folder with the command below:

    :::bash
    npm install --prefix /path/to/local/install kresus

Once you'll have generated a nice [configuration file]({filename}02-admin.md#with-a-configuration-file),
Kresus will be executable with the command:

    :::bash
    /path/to/local/install/node_modules/bin/kresus --config /path/to/config.ini
