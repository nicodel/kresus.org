Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Learn how to install it through different ways: npm, docker, build…
lang: en
status: hidden
is_doc: true

Kresus can be installed according to the way you prefer:

- via [npm]({filename}install-npm.md),
- via [Docker]({filename}install-docker.md),
- via [Yunohost](https://yunohost.org), with a level 7 application,
[application](https://github.com/YunoHost-Apps/kresus_ynh).
- on ArchLinux, using the [pacman package](https://www.archlinux.org/packages/community/x86_64/kresus/): `pacman -Syu kresus`,
    - *⚠︎ This package being maintained by the community, it might not be using
      the latest version of Kresus.*
- from the [sources]({filename}install-sources.md)

## Community tutorials

First of all, let's thank our community for making well-documented and very
complete tutorials explaining how to install Kresus within different environments!

- [fr] [Manage your finances with Kresus](https://wiki.bruno-tatu.com/doku.php?id=wiki:install-kresus)
- [fr] [Kresus, a Web personal finance manager](https://blog.karolak.fr/2016/03/18/kresus-un-gestionnaire-web-de-finances-personnelles/)
- [fr] [Install Kresus on Debian 8](https://carmagnole.ovh/tuto-installer-kresus.htm)
- [fr] [Install Kresus on a Raspberry Pi 3](https://forum.cabane-libre.org/topic/525/installer-kresus-sur-un-raspberry-3)
- [fr] [Install Kresus in a FreeBSD jail](https://wiki.kingpenguin.tk/informatique/serveur/kresus)

If you write any tutorial that would be related to Kresus, let's get in touch:
we would be certainly very happy to include it in this list!
