Title: FAQ
Date: 2017-03-11 10:02
Slug: faq
Summary: FAQ
toc_run: true
toc_title: Frequently Asked Questions
lang: en
status: hidden
is_doc: True

## Security

### How is my banking data stored in Kresus?

If you use the standalone version of Kresus, we assume you know how to secure
your server. No data will be encrypted and this mode, not even your banking
password. Then, we advise you encrypt the hard disk on which Kresus is
installed, or you containerize the web service, or you use any such mean to
secure your password. In the future, the password might be stored encrypted in
this mode as well.

### Why should I give my banking password to Kresus?

To fetch banking operations from your bank website, Kresus fakes a web browser
(thanks to Weboob) and then login to your bank website using your credentials.
This lets him fetch your banking data, in an automated way, every night,
without any action from your part.

### Why does Kresus does not support two-factor authentication?

As Kresus logs in your bank website the same way as you would do through your
browser, but it does it every night, it should ask you every time to type your
two-factor authentication login, which is an obvious issue : Kresus would no
longer be autonomous.

## Synchronization with the bank

### When and how does Kresus update my transactions?

Upon a bank access creation Kresus downloads from your bank's website the totality of the
transactions available.

Then Kresus downloads the new transactions during the night (the hour is not fixed).
However, the banks will sometimes add new transactions several days after their real date, therefore
Kresus will retrieve the transactions up to one month before the last fetch, to be sure.
You can tune this threshold in the administration if you feel that is not enough.

## About Kresus

### I have a question which is not in this list but should be, in my opinion.

Great! Please ask it on our [community website](http://community.kresus.org/)
or [open an issue](https://framagit.org/kresusapp/kresus.org/issues/new) on our
bug tracker directly. If you have a beginning of answer, it's even better! If
you are wondering about something, chances are you are not the only one!

### How to report a bug?

Kresus is made by humans, and can then have some bugs! If you ever find an
issue while using Kresus, feel free to [open an
issue](https://framagit.org/kresusapp/kresus/issues/new) on our bug tracker.
Try to include as much information as you can on your setup: which Kresus
version are you using? Which Weboob version are you using? What are the steps
to reproduce the bug? The more information we have, the greater the chances to
see the bug fixed quickly!

### I'd like to have a new feature.

We are never short of ideas, and they are always greatly appreciated! You can
also [open an issue](https://framagit.org/kresusapp/kresus.org/issues/new) for
a feature request and even start to implement it! Maintainers are always happy
to help someone get started working on Kresus.
